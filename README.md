# Test project for Unilimes

## Getting started

1. Download files to your server
2. Navigate to folder with docker-compose.yml
3. Exec command docker compose up 
4. Open in browser \<your ip\>:14081/src/Pages/importData.php
5. Wait until the end of import, you will see text 'Import completed'
6. Open in browser \<your ip\>:14081/src/Pages/table.php
7. Write desired conditions to input fields, then press 'Search'
8. Browse data by clicking navigation buttons after main table
9. Click 'Export to csv' for csv file


### Writen by Kostomakha Anton