<?php
ini_set('max_execution_time', 0);

use Entity\Client;

include($_SERVER['DOCUMENT_ROOT'] . '/src/Entity/Client.php');
include($_SERVER['DOCUMENT_ROOT'] . '/src/Model/ClientModel.php');
include($_SERVER['DOCUMENT_ROOT'] . '/src/Utils/Db.php');

$filePath = $_SERVER['DOCUMENT_ROOT'] . "/data/dataset.txt";
if (!file_exists($filePath)) {
	echo "No file found " . $filePath;
} else {
	$db = new \Utils\Db();
	if (!$db->tableExists("Clients")) {
		$db->query("CREATE TABLE IF NOT EXISTS `Clients` (
	  `c_id` int(11) NOT NULL,
	  `c_category` varchar(30) DEFAULT NULL,
	  `c_firstName` text,
	  `c_lastName` text,
	  `c_email` text,
	  `c_gender` tinyint(1) DEFAULT NULL,
	  `c_birth_date` date DEFAULT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		$db->query("ALTER TABLE `Clients`
	  ADD PRIMARY KEY (`c_id`),
	  ADD KEY `c_category` (`c_category`);");

		$db->query("ALTER TABLE `Clients`
	  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT;");
		echo "<p>Created table Clients</p>";
	} else {
		$db->query("TRUNCATE Clients");
		echo "<p>Cleared table Clients</p>";
	}
	$ClientModel = new \Model\ClientModel($db);
	$file_handle = fopen($filePath, "r");
	$isFirstLine = true;
	$clientNum = 1;
	while (!feof($file_handle)) {
		$line = fgets($file_handle);
		if ($isFirstLine) {
			$isFirstLine = false;
		} else {
			if ($line != "") {
				$line = explode(",", $line);

				$Client          = new Client();
				$canImportPerson = true;
				$Client->setCategory(strip_tags($line[0]));
				$Client->setFirstName(strip_tags($line[1]));
				$Client->setLastName(strip_tags($line[2]));
				$Client->setEmail(strip_tags($line[3]));
				if ($line[4] == "male") {
					$Client->setGender(1);
				} elseif ($line[4] == "female") {
					$Client->setGender(0);
				} else {
					$canImportPerson = false;
				}
				$dateOfBirth = date_create($line[5]);
				if ($dateOfBirth === false) {
					$canImportPerson = false;
					// Or create current date
				}
				$Client->setDateOfBirth($dateOfBirth);
				if ($canImportPerson) {
					echo "Imported person: " . $clientNum++ . " <b>" . $Client->getFirstName() . " " . $Client->getLastName() . "</b>, email: <b>" . $Client->getEmail() . "</b>, favorite category: <b>" . $Client->getCategory()
						. "</b>, gender: <b>" . $Client->getGenderText() . "</b> date of birth: <b>" . $Client->getDateOfBirthFormat("d.m.Y") . "</b><br>";
					$ClientModel->save($Client);
				} else {
					echo "Can't import client: <pre>" . print_r($line, 1) . "</pre>";
				}
			}
		}
	}
	$clientNum--;
	echo "Import completed, imported ".$clientNum." clients<br>";
}
