<?php
include($_SERVER['DOCUMENT_ROOT'] . '/src/Entity/Client.php');
include($_SERVER['DOCUMENT_ROOT'] . '/src/Model/ClientModel.php');
include($_SERVER['DOCUMENT_ROOT'] . '/src/Utils/Db.php');


echo "<html lang='en'><head>
<script src='/Public/scripts/table.js'></script>
<link href='/Public/styles/general.css' rel='stylesheet'>
<title>Search and export</title></head><body>";
$db = new \Utils\Db();
if ($db->tableExists("Clients")) {
	$ClientModel = new \Model\ClientModel($db);
	$presets     = $ClientModel->setConditions($_GET);
	$page        = 1;
	if (array_key_exists("page", $_GET) and intval($_GET['page']) != 0) {
		$page = intval($_GET['page']);
	}
	$limit                      = 100;
	$form_gender_male_checked   = ($presets['gender'] == "Male") ? 'selected' : '';
	$form_gender_female_checked = ($presets['gender'] == "Female") ? 'selected' : '';
	$form_gender_any_checked    = ($presets['gender'] == "Any") ? 'selected' : '';
	$allCats = $db->query("SELECT DISTINCT `c_category` FROM `Clients` ORDER BY `c_category` ASC");
	$allCatsText = [];
	if ($allCats !== true){
		foreach ($allCats as $cat){
			$allCatsText[] = "<a href='#' onclick='document.forms.searchform.category.value = \"".$cat['c_category']."\";'>".$cat['c_category']."</a>";
		}
	}
	/*
	 * additional filters
	 * <div class='control'><label class='form-label'>First name</label>  <input type='text' name='first_name' value='" . $presets['first_name'] . "'></div>
<div class='control'><label class='form-label'>Last name</label> <input type='text' name='last_name' value='" . $presets['last_name'] . "'></div>
	<div class='control'><label class='form-label'>Email</label> <input type='text' name='email' value='" . $presets['email'] . "'></div>
	 * */
	echo "<form method='get' name='searchform'>
<div class='control'><label class='form-label'>Gender</label> <select name='gender'><option " . $form_gender_any_checked . ">Any</option>
<option " . $form_gender_male_checked . ">Male</option><option " . $form_gender_female_checked . ">Female</option></select></div>

<div class='control'><label class='form-label'>Category</label> <input type='text' name='category' value='" . $presets['category'] . "'></div>
Possible categories: ".implode(", ", $allCatsText)."

<div class='control'><label class='form-label'>Date of birth</label> <input type='text' name='date_of_birth' value='" . $presets['date_of_birth'] . "'> Format dd.mm.yyyy</div>
<div class='control'><label class='form-label'>Age</label> <input type='text' name='age' value='" . $presets['age'] . "'></div>
<div class='control'><label class='form-label'>Age min</label> <input type='text' name='age_min' value='" . $presets['age_min'] . "'> works if field 'age' not set</div>
<div class='control'><label class='form-label'>Age max</label> <input type='text' name='age_max' value='" . $presets['age_max'] . "'> works if field 'age' not set</div>
<button type='submit'>Search</button>
</form>";
	if ($presets['Error'] != "") {
		echo "<p>" . $presets['Error'] . "</p>";
	}
	$ClientModel->setLimit($limit);
	$answer = $ClientModel->find(($page - 1));
	if (empty($answer)) {
		echo "No clients found";
	} else {
		echo "<table><thead><th>Name</th><th>Last Name</th><th>E-mail</th><th>Favorite Category</th><th>Gender</th><th>Date of Birth</th><th>Age</th></thead><tbody>";
		/** @var \Entity\Client $Client */
		foreach ($answer as $Client) {
			echo "<tr>";
			echo "<td>" . $Client->getFirstName() . "</td>";
			echo "<td>" . $Client->getLastName() . "</td>";
			echo "<td>" . $Client->getEmail() . "</td>";
			echo "<td>" . $Client->getCategory() . "</td>";
			echo "<td>" . $Client->getGenderText() . "</td>";
			echo "<td>" . $Client->getDateOfBirthFormat("d.m.Y") . "</td>";
			echo "<td>" . $Client->getAge() . "</td>";
			echo "</tr>";
		}
		echo "</tbody></table>";
	}
	$count = $ClientModel->count();
	if ($count >0){
		$pageNum = intval($count/$limit);
		echo "<form method='get' name='hiddenform'>";
		foreach ($presets as $name => $val) {
			echo "<input type='hidden' name='" . $name . "' value='" . $val . "'>";
		}
		echo "<input type='hidden' name='page' value='" . $page . "'></form>";
		echo "Total results " . $count . " ";
		if ($page > 2) {
			echo " <button onclick='SetPage(1)'>First page</button>";
		}
		if ($page > 1) {
			echo " <button onclick='SetPage(" . ($page - 1) . ");'><< Prev. page " . ($page - 1) . "</button>";
		}
		echo " Current page: " . $page;
		if ($page < $pageNum){
			echo " <button onclick='SetPage(" . ($page + 1) . ");'>Next page " . ($page + 1) . " >></button>";
		}
		if ($page < ($pageNum-1)){
			echo " <button onclick='SetPage(" . $pageNum . ");'>Last page</button> ";
		}


		echo "<form method='post' action='/src/Export/ClientsToCsv.php'>";
		foreach ($presets as $name => $val) {
			echo "<input type='hidden' name='" . $name . "' value='" . $val . "'>";
		}
		echo "<button type='submit'>Export to csv</button>
		</form>";
	}

} else {
	echo "Can't find client table";
}
echo "</body>";