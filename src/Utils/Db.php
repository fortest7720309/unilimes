<?php

namespace Utils;

use PDO;
use PDOException;
use PDOStatement;

class Db
{
	const BASE_NAME = "test_base";
	public ?PDO $pdo = null;

	function __construct() {
		try {
			$this->pdo = new PDO('mysql:host=testmariadb;port=3306;dbname=' .
				$this::BASE_NAME, "test_user", "test_pass", array(
				PDO::ATTR_PERSISTENT       => false,
				PDO::ATTR_EMULATE_PREPARES => true
			));
			$this->pdo->query('SET NAMES utf8');
		} catch (PDOException $e) {
			var_dump($e);
			die("Can't connect to database");
		}
	}

	function quote($arg) {
		$arg = $this->pdo->quote($arg);

		return $arg;
	}

	function query($query) {
		if (!is_string($query) || strlen($query) == 0) {
			return false;
		}
		$result = false;
		try {
			$res    = $this->pdo->query($query);
		} catch (PDOException $e){
			var_dump($e);
			echo "<br>";
			echo $query;
			echo "<br>";
			$res = null;
		}

		//$this->techlog   .= "<br>" . $query;
		if ($res instanceof PDOStatement) {
			$result = array();
			while ($row = $res->fetch(PDO::FETCH_ASSOC)) {
				array_push($result, $row);
			}
			if (empty($result)) {
				$result = true;
			}
		}

		return $result;
	}

	function InsertRow($table_name, $form_data) {
		$fields = array_keys($form_data);
		$dataq  = array();
		foreach ($form_data as $f) {
			$dataq[] = $this->quote($f);
		}
		// build the query
		$sql = "INSERT INTO " . $table_name . "
    (`" . implode("`,`", $fields) . "`)
    VALUES(" . implode(",", $dataq) . ")";

		// run and return the query result resource
		$id = $this->modifyRequest($sql);

		return $id;
	}

	public function modifyRequest($query) {
		if (!is_string($query) || strlen($query) == 0) {
			return false;
		}
		$result = false;
		$res    = $this->pdo->query($query);
		if (!($res instanceof PDOStatement)) {
			$errorInfo = $this->pdo->errorInfo();
			echo $errorInfo[2];
			echo $query;
		} else {
			$result = intval($this->pdo->lastInsertId());
			if ($result == 0) {
				$result = true;
			}
		}

		return $result;
	}

	public function tableExists(string $tableName): bool {
		$table_exist = $this->query("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = ".$this->quote($tableName)." AND TABLE_SCHEMA = '".$this::BASE_NAME."'");
		if ($table_exist !== true) {
			return true;
		}
		return false;
	}
}