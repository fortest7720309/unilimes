<?php
include($_SERVER['DOCUMENT_ROOT'] . '/src/Entity/Client.php');
include($_SERVER['DOCUMENT_ROOT'] . '/src/Model/ClientModel.php');
include($_SERVER['DOCUMENT_ROOT'] . '/src/Utils/Db.php');

$db = new \Utils\Db();
if ($db->tableExists("Clients")) {
	$ClientModel = new \Model\ClientModel($db);
	$ClientModel->setLimit(0);
	$ClientModel->setConditions($_POST);
	$answer = $ClientModel->find(0);
	if (empty($answer)){
		echo "No clients found";
	} else {
		header('Content-Disposition: attachment;filename="clients.csv"');
		/** @var \Entity\Client $Client */
		foreach ($answer as $Client){

			echo $Client->getFirstName().";";
			echo $Client->getLastName().";";
			echo $Client->getEmail().";";
			echo $Client->getCategory().";";
			echo $Client->getGenderText().";";
			echo $Client->getDateOfBirthFormat("d.m.Y").";\n";

		}

	}
} else {
	echo "Can't find client table";
}