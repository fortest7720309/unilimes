<?php

namespace Entity;

class Client
{
var int $id = 0;
var string $category;
var string $firstName;
var string $lastName;
var string $email;
var \DateTime $dateOfBirth;
var bool $gender;

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id): void {
		$this->id = $id;
	}


	/**
	 * @return string
	 */
	public function getCategory(): string {
		return $this->category;
	}

	/**
	 * @param string $category
	 */
	public function setCategory(string $category): void {
		$this->category = $category;
	}

	/**
	 * @return string
	 */
	public function getFirstName(): string {
		return $this->firstName;
	}

	/**
	 * @param string $name
	 */
	public function setFirstName(string $name): void {
		$this->firstName = $name;
	}

	/**
	 * @return string
	 */
	public function getLastName(): string {
		return $this->lastName;
	}

	/**
	 * @param string $surName
	 */
	public function setLastName(string $surName): void {
		$this->lastName = $surName;
	}

	/**
	 * @return string
	 */
	public function getEmail(): string {
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail(string $email): void {
		$this->email = $email;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateOfBirth(): \DateTime {
		return $this->dateOfBirth;
	}

	/**
	 * @param \DateTime $dateOfBirth
	 */
	public function setDateOfBirth(\DateTime $dateOfBirth): void {
		$this->dateOfBirth = $dateOfBirth;
	}

	/**
	 * @return bool
	 */
	public function getGender(): bool {
		return $this->gender;
	}

	/**
	 * @param bool $gender
	 */
	public function setGender(bool $gender): void {
		$this->gender = $gender;
	}

	public function getGenderText(): string {
		if ($this->getGender() == 1){
			return "Male";
		} elseif ($this->getGender() == 0){
			return "Female";
		}
		return "-";
	}

	public function getDateOfBirthFormat(string $format): string {
		return $this->dateOfBirth->format($format);
	}

	public function getAge():int {
		$now = new \DateTime();
		$interval = $now->diff($this->getDateOfBirth());
		return $interval->y;
	}
}