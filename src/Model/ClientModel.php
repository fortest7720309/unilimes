<?php

namespace Model;

use Entity\Client;
use Utils\Db;

class ClientModel
{
	var Db $db;
	var array $where = [];
	var int $limit = 0;

	/**
	 * @param Db $db
	 */
	public function __construct(Db $db) {
		$this->db = $db;
	}

	public function save(Client $client) {
		$data = [];
		if ($client->getId() != 0) {
			$data['c_id'] = $client->getId();
		}
		$data['c_category']  = $client->getCategory();
		$data['c_firstName'] = $client->getFirstName();
		$data['c_lastName']  = $client->getLastName();
		$data['c_email']     = $client->getEmail();
		if ($client->getGender() == false) {
			$data['c_gender'] = 0;//female
		} else {
			$data['c_gender'] = 1;
		}
		$data['c_birth_date'] = $client->getDateOfBirthFormat("Y-m-d");
		$this->db->InsertRow("Clients", $data);
	}

	public function find(int $page): array {
		$skip      = $page * $this->getLimit();
		$where     = $this->GenerateWhere();
		$queryText = "SELECT * FROM `Clients` " . $where;
		if ($this->getLimit() > 0) {
			$queryText .= " LIMIT " . intval($skip) . "," . intval($this->getLimit());
		}
		$temp   = $this->db->query($queryText);
		$answer = [];
		if ($temp !== true and $temp !== false) {
			foreach ($temp as $data) {
				$Client = new Client();
				$Client->setId($data['c_id']);
				$Client->setCategory($data['c_category']);
				$Client->setFirstName($data['c_firstName']);
				$Client->setLastName($data['c_lastName']);
				$Client->setEmail($data['c_email']);
				if ($data['c_gender'] == 0) {
					$Client->setGender(0);
				} else {
					$Client->setGender(1);
				}
				$Client->setDateOfBirth(date_create($data['c_birth_date']));
				$answer[] = $Client;
			}
		}

		return $answer;
	}


	public function count() {
		$where = $this->GenerateWhere();

		return $this->db->query("SELECT COUNT(*) as count FROM `Clients` " . $where)[0]['count'];
	}

	public function WhereFirstName(string $form_FirstName) {
		$this->where['c_firstName'] = ['value' => $this->db->quote("%" . $form_FirstName . "%"), 'type' => "LIKE"];
	}

	public function WhereLastName(mixed $form_LastName) {
		$this->where['c_lastName'] = ['value' => $this->db->quote("%" . $form_LastName . "%"), 'type' => "LIKE"];
	}

	public function WhereCategory(mixed $form_Category) {
		$this->where['c_category'] = ['value' => $this->db->quote("%" . $form_Category . "%"), 'type' => "LIKE"];
	}

	public function WhereEmail(mixed $form_Email) {
		$this->where['c_email'] = ['value' => $this->db->quote("%" . $form_Email . "%"), 'type' => "LIKE"];
	}

	public function WhereDateOfBirth(mixed $form_DateOfBirth) {
		$form_DateOfBirth = date_create_from_format("d.m.Y", $form_DateOfBirth);
		if ($form_DateOfBirth !== false) {
			$form_DateOfBirth            = date_format($form_DateOfBirth, "Y-m-d");
			$this->where['c_birth_date'] = ['value' => $this->db->quote($form_DateOfBirth), 'type' => "="];
		}
	}

	public function WhereDateOfBirthBetween(\DateTime $date_start, \DateTime $date_end) {
		$this->where['c_birth_date'] = ['value_min' => $this->db->quote($date_start->format("Y-m-d")),
		                                'value_max' => $this->db->quote($date_end->format("Y-m-d")), 'type' => "BETWEEN"];
	}

	public function WhereDateOfBirthBefore(\DateTime $date_end) {
		$this->where['c_birth_date'] = ['value' => $this->db->quote($date_end->format("Y-m-d")), 'type' => "<"];
	}

	public function WhereDateOfBirthAfter(\DateTime $date_start) {
		$this->where['c_birth_date'] = ['value' => $this->db->quote($date_start->format("Y-m-d")), 'type' => ">"];
	}

	public function WhereGender(mixed $form_Gender) {
		if ($form_Gender == "Any") {
			$this->where['c_gender'] = ['value' => "", 'type' => "="];
		} elseif ($form_Gender == "Male") {
			$this->where['c_gender'] = ['value' => "1", 'type' => "="];
		} elseif ($form_Gender == "Female") {
			$this->where['c_gender'] = ['value' => "0", 'type' => "="];
		}

	}

	private function GenerateWhere() {
		$where = [];
		foreach ($this->where as $field => $arr) {
			if ($arr['type'] == "BETWEEN") {
				$where[] .= $field . " " ."BETWEEN ". $arr['value_min']. " AND ".$arr['value_max'];
			} elseif ($arr['value'] != "") {
				$where[] .= $field . " " . $arr['type'] . " " . $arr['value'];
			}
		}
		if (empty($where)) {
			return "";
		} else {
			$where = implode(" AND ", $where);

			//	echo $where;
			return "WHERE " . $where;
		}
	}

	/**
	 * @return int
	 */
	public function getLimit(): int {
		return $this->limit;
	}

	/**
	 * @param int $limit
	 */
	public function setLimit(int $limit): void {
		$this->limit = $limit;
	}

	public function setConditions(array $arr) {
		$presets          = [];
		$presets['Error'] = "";
		if (array_key_exists("first_name", $arr) and $arr['first_name'] != "") {
			$presets['first_name'] = trim(strip_tags($arr['first_name']));
			$this->WhereFirstName($presets['first_name']);
		} else {
			$presets['first_name'] = "";
		}
		if (array_key_exists("last_name", $arr) and $arr['last_name'] != "") {
			$presets['last_name'] = trim(strip_tags($arr['last_name']));
			$this->WhereLastName($presets['last_name']);
		} else {
			$presets['last_name'] = "";
		}
		if (array_key_exists("category", $arr) and $arr['category'] != "") {
			$presets['category'] = trim(strip_tags($arr['category']));
			$this->WhereCategory($presets['category']);
		} else {
			$presets['category'] = "";
		}
		if (array_key_exists("email", $arr) and $arr['email'] != "") {
			$presets['email'] = trim(strip_tags($arr['email']));
			$this->WhereEmail($presets['email']);
		} else {
			$presets['email'] = "";
		}
		if (array_key_exists("date_of_birth", $arr) and $arr['date_of_birth'] != "") {
			$presets['date_of_birth'] = trim(strip_tags($arr['date_of_birth']));
			$date                     = explode(".", $presets['date_of_birth']);
			if (checkdate($date[1], $date[0], $date[2])) {
				$this->WhereDateOfBirth($presets['date_of_birth']);
			} else {
				$presets['date_of_birth'] = "";
				$presets['Error']         .= "Wrong date format";
			}

		} else {
			$presets['date_of_birth'] = "";
		}
		if (array_key_exists("gender", $arr) and $arr['gender'] != "" and $arr['gender'] != "Any") {
			$presets['gender'] = trim(strip_tags($arr['gender']));
			$this->WhereGender($presets['gender']);
		} else {
			$presets['gender'] = "";
		}
		if (array_key_exists("age", $arr) and $arr['age'] != "") {
			$presets['age'] = intval($arr['age']);
		} else {
			$presets['age'] = "";
		}
		if (array_key_exists("age_min", $arr) and $arr['age_min'] != "") {
			$presets['age_min'] = intval($arr['age_min']);
		} else {
			$presets['age_min'] = "";
		}
		if (array_key_exists("age_max", $arr) and $arr['age_max'] != "") {
			$presets['age_max'] = intval($arr['age_max']);
		} else {
			$presets['age_max'] = "";
		}
		if ($presets['age'] != "") {
			$date_start = new \DateTime("now");
			$date_start->modify("-".intval($presets['age'])." years");
			$date_start->modify("-364 days");
			$date_end = new \DateTime("now");
			$date_end->modify("-".intval($presets['age'])." years");
			$this->WhereDateOfBirthBetween($date_start,$date_end);
		} elseif ($presets['age_min'] != "" and $presets['age_max'] != "") {
			$date_start = new \DateTime("now");
			$date_start->modify("-".intval($presets['age_max'])." years");
			$date_start->modify("-364 days");
			$date_end = new \DateTime("now");
			$date_end->modify("-".intval($presets['age_min'])." years");
			$this->WhereDateOfBirthBetween($date_start,$date_end);
		} elseif ($presets['age_min'] != "") {
			$date_start = new \DateTime("now");
			$date_start->modify("-".intval($presets['age_min'])." years");
			$this->WhereDateOfBirthBefore($date_start);
		} elseif ($presets['age_max'] != "") {
			$date_end = new \DateTime("now");
			$date_end->modify("-".intval($presets['age_max'])." years");
			$date_end->modify("-364 days");
			$this->WhereDateOfBirthAfter($date_end);
		}

		return $presets;
	}


}